export interface Social {
  gitlab: string
  github: string
  twitter: string
  linkedin: string
}

export interface Profile {
  name: string
  lastname: string
  description: string
  profileImg: string
  social: Social
}
