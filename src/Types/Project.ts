export interface Project {
  name: string
  technologies: string
  liveProject: string
  sourceProject: string
  thumbnail: string
}
