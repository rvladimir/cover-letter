import type { FC } from 'react'
import Section from '../../components/Section/Section'
import Divisor from '../../components/Divisor/Divisor'

import './SkillsStyles.css'
import {
  AwsIcon,
  CssIcon,
  DockerIcon,
  GitIcon,
  HtmlIcon,
  JavaIcon,
  JsIcon,
  KafkaIcon,
  KotlinIcon,
  NodeIcon,
  PythonIcon,
  ReactIcon,
  SpringIcon,
  TsIcon,
  VueIcon
} from '../../components/Icons'

const Skills: FC = () => {
  const languagesProgramming = (
    <>
      <span className="heading-l-font title-skills">Programming Languages</span>
      <div className="container-skills">
        <JsIcon size="medium" />
        <TsIcon size="medium" />
        <JavaIcon size="medium" />
        <KotlinIcon size="medium" />
        <PythonIcon size="medium" />
      </div>
    </>
  )

  const webTechnologies = (
    <>
      <div>
        <span className="heading-l-font title-skills">Web Technologies</span>
      </div>
      <div className="container-skills">
        <HtmlIcon size="medium" />
        <CssIcon size="medium" />
      </div>
    </>
  )

  const frameworksLibraries = (
    <>
      <div>
        <span className="heading-l-font title-skills">Frameworks</span>
      </div>
      <div className="container-skills">
        <ReactIcon size="medium" />
        <VueIcon size="medium" />
        <NodeIcon size="big" />
        <SpringIcon size="big" />
      </div>
    </>
  )

  const otherTools = (
    <>
      <div>
        <span className="heading-l-font title-skills">Other tools</span>
      </div>
      <div className="container-skills">
        <KafkaIcon size="big" />
        <AwsIcon size="medium" />
        <DockerIcon size="medium" />
        <GitIcon size="medium" />
      </div>
    </>
  )

  const content = (
    <>
      <Divisor />
      {languagesProgramming}
      {frameworksLibraries}
      {webTechnologies}
      {otherTools}
    </>
  )

  return <Section>{content}</Section>
}

export default Skills
