import type { FC } from 'react'
import Section from '../../components/Section/Section'
import type { Profile } from '../../Types/Profile'

import './IntroductionStyles.css'

interface IntroductionProps {
  profile: Profile
}

const Introduction: FC<IntroductionProps> = ({ profile }) => {
  const tittle = (
    <>
      <span className="heading-xl-font">Nice to meet you!</span>
      <br></br>
      <span className="heading-xl-font">Hello I'm </span>
      <span className="heading-xl-font-underline">{profile.name}</span>
      <br></br>
    </>
  )

  const description = (
    <div className="description-introduction">
      <span className="body-font-green">{profile.description}</span>
    </div>
  )

  const profileImage = (
    <img
      className="profile-img"
      src={`/images/${profile.profileImg}`}
      alt="rvladimir image profile"
    />
  )

  const content = (
    <div className="container-introduction">
      <div id="text-introduction">
        {tittle}
        {description}
      </div>
      {profileImage}
    </div>
  )

  return <Section>{content}</Section>
}

export default Introduction
