import type { FC } from 'react'
import Section from '../../components/Section/Section'
import Card from '../../components/Card/Card'

import './ProjectsStyles.css'
import type { Project } from '../../Types/Project'

interface ProjectsProps {
  projects: Project[]
}

const Projects: FC<ProjectsProps> = ({ projects }) => {
  const content = (
    <div className="container-projects">
      <span className="heading-xl-font">Projects</span>
      <div className="container-cards">
        {projects.map((project) => (
          <Card project={project} key={project.name} />
        ))}
      </div>
    </div>
  )

  return <Section>{content}</Section>
}

export default Projects
