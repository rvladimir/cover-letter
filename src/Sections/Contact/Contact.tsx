import type { FC } from 'react'
import './ContactStyles.css'

interface ContactProps {
  text: string
  linkedinUrl: string
}

const Contact: FC<ContactProps> = ({ text, linkedinUrl }) => {
  return (
    <div className="container-contact">
      <span className="heading-xl-font contact-title">Contact</span>
      <div className="body-font contact-body">
        {`${text} `}
        <a className="body-font-underline" href={linkedinUrl} target="_blank">
          Linkedin!
        </a>
      </div>
    </div>
  )
}

export default Contact
