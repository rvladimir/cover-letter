import type { FC } from 'react'
import type { Project } from '../../Types/Project'

import './CardStyles.css'

interface CardProps {
  project: Project
}

const Card: FC<CardProps> = ({ project }) => {
  const cardImage = (
    <img
      className="card-image"
      src={`/images/${project.thumbnail}`}
      alt={`img-card-${project.thumbnail}`}
      key={project.thumbnail}
    />
  )

  const titleCard = (
    <div className="card-title">
      <span className="heading-m-font upperCaseFont">{project.name}</span>
    </div>
  )

  const descriptionCard = (
    <div className="card-description">
      <span className="body-font-green upperCaseFont">
        {project.technologies}
      </span>
    </div>
  )

  const actionsCard = (
    <div className="card-actions">
      <a
        className="body-font-underline"
        href={project.liveProject}
        target="_blank"
      >
        VIEW PROJECT
      </a>
      <a
        className="body-font-underline"
        href={project.sourceProject}
        target="_blank"
      >
        VIEW CODE
      </a>
    </div>
  )

  const cardContent = (
    <div className="card-content">
      {cardImage}
      {titleCard}
      {descriptionCard}
      {actionsCard}
    </div>
  )

  return <>{cardContent}</>
}
export default Card
