import type { FC, ReactNode } from 'react'
import './SectionStyles.css'

interface SectionProps {
  children: ReactNode
}

const Section: FC<SectionProps> = ({ children }) => {
  return <div className="container-section">{children}</div>
}

export default Section
