import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import KotlinSvg from '../../assets/icons/kotlin-icon.svg'

const KotlinIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={KotlinSvg} size={size} />
}

export default KotlinIcon
