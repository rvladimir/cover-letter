import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import DockerSvg from '../../assets/icons/docker-icon.svg'

const DockerIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={DockerSvg} size={size} />
}

export default DockerIcon
