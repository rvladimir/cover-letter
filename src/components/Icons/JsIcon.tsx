import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import JsSvg from '../../assets/icons/js-icon.svg'

const JsIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={JsSvg} size={size} />
}

export default JsIcon
