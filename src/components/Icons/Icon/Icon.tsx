import type { FC } from 'react'
import './IconStyles.css'
import type { ImageMetadata } from 'astro'

interface IconProps {
  image: ImageMetadata
  size: 'small' | 'medium' | 'big'
}

const Icon: FC<IconProps> = ({ image, size }) => {
  return <img className={`icon-${size}`} src={image.src} alt={image.src} />
}

export default Icon
