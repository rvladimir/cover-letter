interface IconProps {
  size: 'small' | 'medium' | 'big'
}

export default IconProps
