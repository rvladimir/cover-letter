import type { FC } from 'react'
import Icon from './Icon/Icon'
import TwitterSvg from '../../assets/icons/twitter-icon.svg'
import type IconProps from './Icon/IconProps'

const TwitterIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={TwitterSvg} size={size} />
}

export default TwitterIcon
