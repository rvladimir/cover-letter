import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import PythonSvg from '../../assets/icons/python-icon.svg'

const PythonIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={PythonSvg} size={size} />
}

export default PythonIcon
