import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import GitSvg from '../../assets/icons/git-icon.svg'

const GitIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={GitSvg} size={size} />
}

export default GitIcon
