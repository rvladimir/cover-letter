import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import KafkaSvg from '../../assets/icons/kafka-icon.svg'

const KafkaIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={KafkaSvg} size={size} />
}

export default KafkaIcon
