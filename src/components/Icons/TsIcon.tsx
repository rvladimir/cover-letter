import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import TsSvg from '../../assets/icons/ts-icon.svg'

const TsIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={TsSvg} size={size} />
}

export default TsIcon
