import type { FC } from 'react'
import Icon from './Icon/Icon'
import LinkedinSvg from '../../assets/icons/linkedin-icon.svg'
import type IconProps from './Icon/IconProps'

const LinkedinIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={LinkedinSvg} size={size} />
}

export default LinkedinIcon
