import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import CssSvg from '../../assets/icons/css3-icon.svg'

const CssIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={CssSvg} size={size} />
}

export default CssIcon
