import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import AwsSvg from '../../assets/icons/aws-icon.svg'

const AwsIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={AwsSvg} size={size} />
}

export default AwsIcon
