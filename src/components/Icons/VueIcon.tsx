import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import VueSvg from '../../assets/icons/vue-icon.svg'

const VueIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={VueSvg} size={size} />
}

export default VueIcon
