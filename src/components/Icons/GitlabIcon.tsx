import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import GitlabSvg from '../../assets/icons/gitlab-icon.svg'

const GitlabIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={GitlabSvg} size={size} />
}

export default GitlabIcon
