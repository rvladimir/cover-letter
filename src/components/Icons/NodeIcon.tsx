import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import NodeSvg from '../../assets/icons/nodejs-icon.svg'

const NodeIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={NodeSvg} size={size} />
}

export default NodeIcon
