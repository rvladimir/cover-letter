import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import HtmlSvg from '../../assets/icons/html5-icon.svg'

const HtmlIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={HtmlSvg} size={size} />
}

export default HtmlIcon
