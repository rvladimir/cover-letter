import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import ReactSvg from '../../assets/icons/react-icon.svg'

const ReactIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={ReactSvg} size={size} />
}

export default ReactIcon
