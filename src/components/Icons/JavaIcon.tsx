import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import JavaSvg from '../../assets/icons/java-icon.svg'

const JavaIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={JavaSvg} size={size} />
}

export default JavaIcon
