import type { FC } from 'react'
import Icon from './Icon/Icon'
import GithubSvg from '../../assets/icons/github-icon.svg'
import type IconProps from './Icon/IconProps'

const GithubIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={GithubSvg} size={size} />
}

export default GithubIcon
