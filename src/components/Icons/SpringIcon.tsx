import type { FC } from 'react'
import Icon from './Icon/Icon'
import type IconProps from './Icon/IconProps'
import SpringSvg from '../../assets/icons/spring-icon.svg'

const SpringIcon: FC<IconProps> = ({ size }) => {
  return <Icon image={SpringSvg} size={size} />
}

export default SpringIcon
