import AwsIcon from './AwsIcon'
import CssIcon from './CssIcon'
import DockerIcon from './DockerIcon'
import GitIcon from './GitIcon'
import GithubIcon from './GithubIcon'
import GitlabIcon from './GitlabIcon'
import HtmlIcon from './HtmlIcon'
import JavaIcon from './JavaIcon'
import JsIcon from './JsIcon'
import LinkedinIcon from './LinkedinIcon'
import NodeIcon from './NodeIcon'
import PythonIcon from './PythonIcon'
import ReactIcon from './React'
import SpringIcon from './SpringIcon'
import TsIcon from './TsIcon'
import TwitterIcon from './TwitterIcon'
import VueIcon from './VueIcon'
import KotlinIcon from './KotlinIcon'
import KafkaIcon from './KafkaIcon'

export {
  GithubIcon,
  GitlabIcon,
  TwitterIcon,
  LinkedinIcon,
  TsIcon,
  JsIcon,
  JavaIcon,
  HtmlIcon,
  CssIcon,
  DockerIcon,
  GitIcon,
  ReactIcon,
  VueIcon,
  SpringIcon,
  NodeIcon,
  AwsIcon,
  PythonIcon,
  KafkaIcon,
  KotlinIcon
}
