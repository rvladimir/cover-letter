import type { FC } from 'react'
import './DivisorStyles.css'

const Divisor: FC = () => {
  return (
    <>
      <hr className="divisor-style" />
    </>
  )
}

export default Divisor
