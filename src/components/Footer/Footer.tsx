import type { FC } from 'react'
import Navbar from '../Navbar/Navbar'
import './FooterStyles.css'
import Divisor from '../Divisor/Divisor'
import type { Profile } from '../../Types/Profile'

interface FooterProps {
  profile: Profile
}

const Footer: FC<FooterProps> = ({ profile }) => {
  return (
    <div className="footer-style">
      <Divisor />
      <Navbar profile={profile} />
    </div>
  )
}

export default Footer
