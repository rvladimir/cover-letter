import type { FC } from 'react'
import type { Profile } from '../../Types/Profile'
import { GithubIcon, GitlabIcon, LinkedinIcon, TwitterIcon } from '../Icons'

import './NavbarStyles.css'

interface NavbarProps {
  profile: Profile
}

const Navbar: FC<NavbarProps> = ({ profile }) => {
  return (
    <nav className="navbar">
      <div className="title">
        <span className="heading-m-font">{`${profile.name} ${profile.lastname}`}</span>
      </div>

      <ul className="navbar-items">
        <div className="menu">
          <li>
            <a
              href={profile.social.github}
              aria-label="rvladimir github account"
              target="_blank"
            >
              <span className="body-font">
                <GithubIcon size="small" />
              </span>
            </a>
          </li>
          <li>
            <a
              href={profile.social.gitlab}
              aria-label="rvladimir gitlab account"
              target="_blank"
            >
              <span className="body-font">
                <GitlabIcon size="small" />
              </span>
            </a>
          </li>
          <li>
            <a
              href={profile.social.twitter}
              aria-label="rvladimir twitter account"
              target="_blank"
            >
              <span className="body-font">
                <TwitterIcon size="small" />
              </span>
            </a>
          </li>
          <li>
            <a
              href={profile.social.linkedin}
              aria-label="rvladimir linkedin account"
              target="_blank"
            >
              <span className="body-font">
                <LinkedinIcon size="small" />
              </span>
            </a>
          </li>
        </div>
      </ul>
    </nav>
  )
}

export default Navbar
