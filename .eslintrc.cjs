module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: ['plugin:astro/recommended', 'standard-with-typescript'],
  ignorePatterns: ['/src/assets/*'],
  overrides: [
    {
      files: ['*.astro'],
      parser: 'astro-eslint-parser',
      parserOptions: {
        parser: '@typescript-eslint/parser',
        extraFileExtensions: ['.astro']
      },
      rules: {}
    },
    {
      files: ['.jsx', '.tsx'],
      parser: '@babel/eslint-parser',
      extends: ['plugin:react/recommended'],
      plugins: ['react'],
      parserOptions: {
        ecmaVersion: 'latest'
      },
      ecmaFeatures: {
        jsx: true,
        modules: true
      },
      rules: {}
    }
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: './tsconfig.json'
  },
  rules: {
    indent: ['error', 2],
    'linebreak-style': ['error', 'windows'],
    quotes: ['error', 'single'],
    semi: ['error', 'never']
  },
  plugins: ['prettier']
}
